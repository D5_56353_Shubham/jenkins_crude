CREATE TABLE emp (
  emp_id INTEGER primary key auto_increment,
  name  varchar(50),
  salary INTEGER,
  age INTEGER
);

INSERT INTO emp (name,salary,age) 
VALUES('sanket',20000,20);
INSERT INTO emp (name,salary,age) 
VALUES('suraj',25000,21);