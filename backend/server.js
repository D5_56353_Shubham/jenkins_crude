const express = require("express");
const routerEmp = require("./routes/emp");
const cors = require("cors");
const app = express();
// // to read jason format
app.use(cors("*"));
app.use(express.json());
app.use("/emp", routerEmp);

app.get("/", (request, response) => {
  response.send("welcome to backend");
});

// // use the router to find all the apis rellated to emp

app.listen(4000, () => {
  console.log(`server started on port 4000`);
});
