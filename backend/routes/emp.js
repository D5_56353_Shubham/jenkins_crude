const { response, request } = require("express");
const express = require("express");

const db = require("../db");

const utils = require("../utils");

const router = express.Router();

router.get("/", (request, response) => {
  const statement = `select * from emp`;
  // const connection = db.pool();
  db.execute(statement, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});
//1. Add emp
router.post("/add_emp", (request, response) => {
  const { emp_id, name, salary, age } = request.body;
  // const connection = db.pool();
  const statement = `insert into emp (emp_id,name,salary,age) 
values('${emp_id}','${name}','${salary}','${age}')
`;
  db.execute(statement, (error, emp) => {
    // connection.end();
    response.send(utils.createResult(error, emp));
  });
});

//2. Delete emp

router.delete("/delete_emp/:emp_id", (request, response) => {
  const { emp_id } = request.params;
  const statement = `delete from emp
                  where emp_id=${emp_id}`;

  // const connection = db.pool();
  db.execute(statement, (error, result) => {
    // connection.end();
    response.send(utils.createResult(error, result));
  });
});

//3. Find emp by id
router.get("/:emp_id", (request, response) => {
  const { emp_id } = request.params;
  const statement = `select * from emp
     where emp_id=${emp_id}`;
  // const connection = db.pool();
  db.execute(statement, (error, records) => {
    // connection.end();
    if (records.length > 0) {
      response.send(utils.createResult(error, records[0]));
    } else {
      response.send(utils.createResult("emp does not exist"));
    }
  });
});

// 4. Edit specific emp
router.put("/update_emp/:emp_id", (request, response) => {
  const { emp_id } = request.params;
  const { name, salary, age } = request.body;
  const statement = `update emp
    set
    name='${name}',
    salary=${salary},
    age=${age}
   where emp_id=${emp_id}`;
  // const connection = db.pool();
  db.execute(statement, (error, result) => {
    // connection.end();
    response.send(utils.createResult(error, result));
  });
});

module.exports = router;
