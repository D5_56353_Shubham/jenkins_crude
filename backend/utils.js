function createResult(error, data) {
  const result = {};
  if (error) {
    // if there is any error
    result["status"] = "error";
    //result.status='error'
    result["error"] = error;
  } else {
    //when there is no any error i.e for success
    result["status"] = "success";
    result["data"] = data;
  }
  return result;
}

module.exports = {
  createResult,
};
